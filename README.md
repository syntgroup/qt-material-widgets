# Qt Material Design Desktop Widgets [![Language](https://img.shields.io/badge/language-c++-brightgreen.svg)]() [![Join the chat at https://gitter.im/qt-material-widgets/Lobby](https://badges.gitter.im/qt-material-widgets/Lobby.svg)](https://gitter.im/qt-material-widgets/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

YouTube video preview [available here](http://www.youtube.com/watch?v=21UMeNVBPU4).

<table>
  <tbody>
    <tr>
      <td colspan="2" align="center"></td>
    </tr>
    <tr>
      <td>
        App Bar
      </td>
      <td>
        <code>md::AppBar</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/appbar.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Auto Complete
      </td>
      <td>
        <code>md::AutoComplete</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/autocomplete.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Avatar
      </td>
      <td>
        <code>md::Avatar</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/avatar.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Badge
      </td>
      <td>
        <code>md::Badge</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/badge.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Check Box
      </td>
      <td>
        <code>md::CheckBox</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/checkbox.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Circular Progress
      </td>
      <td>
        <code>md::CircularProgress</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/circularprogress.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Dialog
      </td>
      <td>
        <code>md::Dialog</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/dialog.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Drawer
      </td>
      <td>
        <code>md::Drawer</code>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <img src="gifs/drawer.gif" />
      </td>
    </tr>
    <tr>
      <td>
        FAB
      </td>
      <td>
        <code>md::FloatingActionButton</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/fab.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Flat Button
      </td>
      <td>
        <code>md::FlatButton</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/flatbutton.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Icon Button
      </td>
      <td>
        <code>QtMaterialIconButton</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/iconbutton.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Progress
      </td>
      <td>
        <code>md::Progress</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/progress.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Radio Button
      </td>
      <td>
        <code>md::RadioButton</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/radiobutton.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Raised Button
      </td>
      <td>
        <code>md::RaisedButton</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/raisedbutton.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Scroll Bar
      </td>
      <td>
        <code>md::ScrollBar</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/scrollbar.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Slider
      </td>
      <td>
        <code>md::Slider</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/slider.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Snackbar
      </td>
      <td>
        <code>md::SnackBar</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/snackbar.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Tabs
      </td>
      <td>
        <code>md::Tabs</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/tabs.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Text Field
      </td>
      <td>
        <code>md::TextField</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/textfield.gif" />
      </td>
    </tr>
    <tr>
      <td>
        Toggle
      </td>
      <td>
        <code>md::Toggle</code>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <img src="gifs/toggle.gif" />
      </td>
    </tr>
  </tbody>
</table>

#### Implemented components

- [x] App Bar
- [x] Auto Complete
- [x] Avatar
- [x] Badge
- [x] Check Box
- [x] Circular Progress
- [x] Dialog
- [x] Drawer
- [x] Floating Action Button
- [x] Flat Button
- [x] Icon Button
- [x] Progress
- [x] Radio Button
- [x] Raised Button
- [x] Scroll Bar
- [x] Slider
- [x] Snackbar
- [x] Tabs
- [x] Text Field
- [x] Toggle

#### Work in progress

- [ ] Divider
- [ ] List
- [ ] List Item
- [ ] Menu
- [ ] Paper
- [ ] Snackbar Layout
- [ ] Table

#### Not implemented 

- [ ] Card
- [ ] Chips
- [ ] Discrete Slider
- [ ] Grid List
- [ ] Icon Menu
- [ ] Search Field
- [ ] Select Field
- [ ] Stepper
- [ ] Subheaders
- [ ] Toolbar
