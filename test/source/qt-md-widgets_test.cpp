#include <string>

#include "qt-md-widgets/qt-md-widgets.hpp"

auto main() -> int
{
  auto const exported = exported_class {};

  return std::string("qt-md-widgets") == exported.name() ? 0 : 1;
}
