if(PROJECT_IS_TOP_LEVEL)
  set(CMAKE_INSTALL_INCLUDEDIR include/qt-md-widgets CACHE PATH "")
endif()

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

# find_package(<package>) call for consumers to find this project
set(package qt-md-widgets)

install(
    DIRECTORY
    components/
    "${PROJECT_BINARY_DIR}/export/"
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
    COMPONENT qt-md-widgets_Development
)

install(
    TARGETS qt-md-widgets_qt-md-widgets
    EXPORT qt-md-widgetsTargets
    RUNTIME #
    COMPONENT qt-md-widgets_Runtime
    LIBRARY #
    COMPONENT qt-md-widgets_Runtime
    NAMELINK_COMPONENT qt-md-widgets_Development
    ARCHIVE #
    COMPONENT qt-md-widgets_Development
    INCLUDES #
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
)

write_basic_package_version_file(
    "${package}ConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
)

# Allow package maintainers to freely override the path for the configs
set(
    qt-md-widgets_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${package}"
    CACHE PATH "CMake package config location relative to the install prefix"
)
mark_as_advanced(qt-md-widgets_INSTALL_CMAKEDIR)

install(
    FILES cmake/install-config.cmake
    DESTINATION "${qt-md-widgets_INSTALL_CMAKEDIR}"
    RENAME "${package}Config.cmake"
    COMPONENT qt-md-widgets_Development
)

install(
    FILES "${PROJECT_BINARY_DIR}/${package}ConfigVersion.cmake"
    DESTINATION "${qt-md-widgets_INSTALL_CMAKEDIR}"
    COMPONENT qt-md-widgets_Development
)

install(
    EXPORT qt-md-widgetsTargets
    NAMESPACE qt-md-widgets::
    DESTINATION "${qt-md-widgets_INSTALL_CMAKEDIR}"
    COMPONENT qt-md-widgets_Development
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
