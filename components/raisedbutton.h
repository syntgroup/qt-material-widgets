#ifndef RAISEDBUTTON_H
#define RAISEDBUTTON_H

#include "flatbutton.h"
#include "qt-md-widgets/qt-md-widgets_export.hpp"

namespace md
{

class RaisedButtonPrivate;

class QT_MD_WIDGETS_EXPORT RaisedButton : public FlatButton
{
    Q_OBJECT

public:
    explicit RaisedButton(QWidget *parent = 0);
    explicit RaisedButton(const QString &text, QWidget *parent = 0);
    ~RaisedButton();

protected:
    RaisedButton(RaisedButtonPrivate &d, QWidget *parent = 0);

    bool event(QEvent *event) Q_DECL_OVERRIDE;

private:
    Q_DISABLE_COPY(RaisedButton)
    Q_DECLARE_PRIVATE(RaisedButton)
};
}
#endif // RAISEDBUTTON_H
