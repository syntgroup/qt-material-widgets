#ifndef RADIOBUTTON_H
#define RADIOBUTTON_H

#include "lib/checkable.h"
#include "qt-md-widgets/qt-md-widgets_export.hpp"

namespace md
{

class RadioButtonPrivate;

class QT_MD_WIDGETS_EXPORT RadioButton : public Checkable
{
    Q_OBJECT

public:
    explicit RadioButton(QWidget *parent = 0);
    ~RadioButton();

protected:
    void setupProperties();

private:
    Q_DISABLE_COPY(RadioButton)
    Q_DECLARE_PRIVATE(RadioButton)
};
}
#endif // RADIOBUTTON_H
